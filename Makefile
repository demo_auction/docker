#init: init-ci frontend-ready
init: docker-down-clear \
	api-clear \
	docker-pull docker-build docker-up \
	api-init
#init-ci: docker-down-clear \
#	api-clear frontend-clear cucumber-clear \
#	docker-pull docker-build docker-up \
#	api-init frontend-init cucumber-init
up: docker-up
down: docker-down
restart: down up
#check: lint analyze validate-schema test test-e2e
check: lint analyze test
#lint: api-lint frontend-lint cucumber-lint
lint: api-lint
analyze: api-analyze
#validate-schema: api-validate-schema
#test: api-test api-fixtures frontend-test
test: api-test
test-unit: api-test-unit
#test-functional: api-test-functional api-fixtures
test-functional: api-test-functional
#test-smoke: api-fixtures cucumber-clear cucumber-smoke
#test-e2e: api-fixtures cucumber-clear cucumber-e2e

#update-deps: api-composer-update frontend-yarn-upgrade cucumber-yarn-upgrade restart

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build --pull

api-clear:
	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c 'rm -rf var/cache/* var/log/* var/test/*'

api-init: api-permissions api-composer-install
#api-init: api-permissions api-composer-install api-wait-db api-migrations api-fixtures

api-permissions:
	docker run --rm -v ${PWD}/api:/app -w /app alpine chmod 777 var/cache var/log var/test

api-composer-install:
	docker-compose run --rm api-php-cli composer i

api-composer-update:
	docker-compose run --rm api-php-cli composer u

api-lint:
	docker-compose run --rm api-php-cli composer lint
	docker-compose run --rm api-php-cli composer cs-check
#	docker-compose run --rm api-php-cli composer php-cs-fixer fix -- --dry-run --diff

api-analyze:
	docker-compose run --rm api-php-cli composer psalm -- --no-diff

api-analyze-diff:
	docker-compose run --rm api-php-cli composer psalm

api-test:
	docker-compose run --rm api-php-cli composer test

api-test-coverage:
	docker-compose run --rm api-php-cli composer test-coverage

api-test-unit:
	docker-compose run --rm api-php-cli composer test -- --testsuite=unit

api-test-unit-coverage:
	docker-compose run --rm api-php-cli composer test-coverage -- --testsuite=unit

api-test-functional:
	docker-compose run --rm api-php-cli composer test -- --testsuite=functional

api-test-functional-coverage:
	docker-compose run --rm api-php-cli composer test-coverage -- --testsuite=functional

# todo: add production build settings
#build: build-gateway build-frontend build-api
#
#build-gateway:
#
#
#build-frontend:
#
#
#build-api:
#

# Тестирование команды make build с переменными окружения
#try-build:
#	REGISTRY=localhost IMAGE_TAG=0 make build

# todo: add production push settings
#push: push-gateway push-frontend push-api
#
#push-gateway:
#
#
#push-frontend:
#
#
#push-api:
#

# todo: deploy && rollback
